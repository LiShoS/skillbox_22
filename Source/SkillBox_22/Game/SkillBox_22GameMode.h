// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillBox_22GameMode.generated.h"

UCLASS(minimalapi)
class ASkillBox_22GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkillBox_22GameMode();
};



