// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "SkillBox_22/FuncLibrary/SupportTypes.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "SkillBox_22PlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class ASkillBox_22Character;

UCLASS()
class ASkillBox_22PlayerController : public APlayerController
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	FMenuInputActions MenuInputActions;

	UPROPERTY(EditDefaultsOnly)
	UInputMappingContext* MenuMappingContext;


	UFUNCTION()
	void CameraZoom(const FInputActionValue& Value);

	ASkillBox_22Character* PossessedCharacter;

public:
	ASkillBox_22PlayerController();

	virtual void BeginPlay() override;

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location);

	//Overriding possession
	virtual void OnPossess(APawn* InPawn) override;

private:
	bool bInputPressed; // Input is bring pressed
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed

};


