// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_22GameMode.h"
#include "SkillBox_22PlayerController.h"
#include "SkillBox_22/Character/SkillBox_22Character.h"
#include "UObject/ConstructorHelpers.h"

ASkillBox_22GameMode::ASkillBox_22GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASkillBox_22PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Game/BP_PC_Base"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}