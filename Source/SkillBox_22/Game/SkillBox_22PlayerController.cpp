// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_22PlayerController.h"

#include "Kismet/KismetSystemLibrary.h"
#include "EnhancedInputSubsystems.h"

#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "SkillBox_22/Character/SkillBox_22Character.h"
#include "Engine/World.h"



ASkillBox_22PlayerController::ASkillBox_22PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ASkillBox_22PlayerController::BeginPlay()
{
	Super::BeginPlay();

		
	{
		// Get the Enhanced Input Local Player Subsystem from the Local Player related to our Player Controller.
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
		{
			// PawnClientRestart can run more than once in an Actor's lifetime, so start by clearing out any leftover mappings.
			//Subsystem->ClearAllMappings();
			if (!Subsystem->HasMappingContext(MenuMappingContext)) {
				// Add each mapping context, along with their priority values. Higher values outprioritize lower values.
				Subsystem->AddMappingContext(MenuMappingContext, 100);
			}
		}
	}

}

void ASkillBox_22PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// if(bInputPressed)
	// {
	// 	FollowTime += DeltaTime;
	//
	// 	// Look for the touch location
	// 	FVector HitLocation = FVector::ZeroVector;
	// 	FHitResult Hit;
	// 	if(bIsTouch)
	// 	{
	// 		GetHitResultUnderFinger(ETouchIndex::Touch1, ECC_Visibility, true, Hit);
	// 	}
	// 	else
	// 	{
	// 		GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	// 	}
	// 	HitLocation = Hit.Location;
	//
	// 	// Direct the Pawn towards that location
	// 	APawn* const MyPawn = GetPawn();
	// 	if(MyPawn)
	// 	{
	// 		FVector WorldDirection = (HitLocation - MyPawn->GetActorLocation()).GetSafeNormal();
	// 		MyPawn->AddMovementInput(WorldDirection, 1.f, false);
	// 	}
	// }
	// else
	// {
	// 	FollowTime = 0.f;
	// }
}

void ASkillBox_22PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//InputComponent->BindAction("SetDestination", IE_Pressed, this, &ASkillBox_22PlayerController::OnSetDestinationPressed);
	//InputComponent->BindAction("SetDestination", IE_Released, this, &ASkillBox_22PlayerController::OnSetDestinationReleased);

	// support touch devices 
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ASkillBox_22PlayerController::OnTouchPressed);
	//InputComponent->BindTouch(EInputEvent::IE_Released, this, &ASkillBox_22PlayerController::OnTouchReleased);

	//Biding enhanced input Events
	if (UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// This calls the handler function on the tick when MyInputAction starts, such as when pressing an action button.
		if (MenuInputActions.CameraZoomAction)
		{
			PlayerEnhancedInputComponent->BindAction(MenuInputActions.CameraZoomAction, ETriggerEvent::Triggered, this, &ASkillBox_22PlayerController::CameraZoom);
		}
	}

}

void ASkillBox_22PlayerController::OnSetDestinationPressed()
{
	// We flag that the input is being pressed
	bInputPressed = true;
	// Just in case the character was moving because of a previous short press we stop it
	StopMovement();
}

void ASkillBox_22PlayerController::OnSetDestinationReleased()
{
	// Player is no longer pressing the input
	bInputPressed = false;

	// If it was a short press
	if(FollowTime <= ShortPressThreshold)
	{
		// We look for the location in the world where the player has pressed the input
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		HitLocation = Hit.Location;

		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, HitLocation);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, HitLocation, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void ASkillBox_22PlayerController::OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = true;
	OnSetDestinationPressed();
}

void ASkillBox_22PlayerController::OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ASkillBox_22PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	//TBR ������� ���������� ��� ����
	PossessedCharacter = dynamic_cast<ASkillBox_22Character*>(InPawn);
}

void ASkillBox_22PlayerController::CameraZoom(const FInputActionValue& Value)
{
	float ZoomValue = .0f;
	if (Value.GetValueType() == EInputActionValueType::Axis1D)
		ZoomValue = Value.Get<float>();

	if (IsValid(PossessedCharacter))
		PossessedCharacter->CameraZoom(ZoomValue);

	//UKismetSystemLibrary::PrintString(this, FString::SanitizeFloat(ZoomValue));
}


