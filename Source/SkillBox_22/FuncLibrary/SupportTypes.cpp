// Fill out your copyright notice in the Description page of Project Settings.


#include "SupportTypes.h"
#include "SkillBox_22/SkillBox_22.h"

bool GetRayAndPlaneIntersection(FVector RayOrigin, FVector RayDirection, float Range,
	FVector PlaneOrigin, FVector PlaneNormal, FVector& IntersectionPoint)
{
	FVector DeltaOrigins = PlaneOrigin - RayOrigin;
	float tmpFloat1 = FVector::DotProduct(DeltaOrigins, PlaneNormal);
	
	FVector tmpVector = RayDirection * Range;
	tmpVector = RayOrigin + tmpVector;
	tmpVector -=  RayOrigin;
	float tmpFloat2 = FVector::DotProduct(tmpVector, PlaneNormal);

	if (tmpFloat2 != 0)
	{
		tmpFloat1 /=  tmpFloat2;
		tmpVector *=  tmpFloat1;
		IntersectionPoint = tmpVector + RayOrigin;
		return  true;
	}
	return false;
}
