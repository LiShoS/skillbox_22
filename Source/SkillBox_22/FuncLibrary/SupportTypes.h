// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "SupportTypes.generated.h"

bool GetRayAndPlaneIntersection(FVector RayOrigin, FVector RayDirection, float Range, FVector PlaneOrigin, FVector PlaneNormal, FVector &IntersectionPoint);

USTRUCT()
struct FCharacterInputActions {

	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	UInputAction* MovementAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* AimAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* FireAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* WalkAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* SprintAction;

};

USTRUCT()
struct FMenuInputActions {
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	UInputAction* CameraZoomAction;

	UPROPERTY(EditDefaultsOnly)
	UInputAction* PauseAction;

};

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Run_State UMETA(DisplayName = "Run State"),
	Walk_State UMETA(DisplayName = "Walk state"),
	Aim_State UMETA(DisplayName = "Aim State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 700.f;
};

UCLASS()
class SKILLBOX_22_API USupportTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
