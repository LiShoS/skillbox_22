// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_22.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillBox_22, "SkillBox_22" );

DEFINE_LOG_CATEGORY(LogSkillBox_22)
 