// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_22Character.h"

#include "Kismet/KismetSystemLibrary.h"
#include "EnhancedInputSubsystems.h"
#include "Math/Vector.h"
#include "Kismet/KismetMathLibrary.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"

#include "Engine/World.h"

ASkillBox_22Character::ASkillBox_22Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}


void ASkillBox_22Character::CharacterUpdate()
{
}

void ASkillBox_22Character::ChangeMovementState()
{
	float NewSpeed = MovementInfo.RunSpeed;

	if (bIsSprining)
	{
		NewSpeed = MovementInfo.SprintSpeed;
	}
	else if (bIsWalking)
	{
		NewSpeed = MovementInfo.WalkSpeed;
	}
	else if (bIsAiming)
	{
		NewSpeed = MovementInfo.AimSpeed;
	}

	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

void ASkillBox_22Character::BeginPlay()
{
	Super::BeginPlay();

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		// Get the Enhanced Input Local Player Subsystem from the Local Player related to our Player Controller.
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			// PawnClientRestart can run more than once in an Actor's lifetime, so start by clearing out any leftover mappings.
			//Subsystem->ClearAllMappings();
			if (!Subsystem->HasMappingContext(InputMappingContext)) {
				// Add each mapping context, along with their priority values. Higher values outprioritize lower values.
				Subsystem->AddMappingContext(InputMappingContext, 0);
			}
		}
	}

	// InitialCameraSetup
	CameraBoom->TargetArmLength = (MaxCameraDistance + MinCameraDistance) / 2;
	FRotator tmpRotator((MaxCameraView.Pitch + MinCameraView.Pitch) / 2,
		(MaxCameraView.Yaw + MinCameraView.Yaw) / 2,
		(MaxCameraView.Roll + MinCameraView.Roll) / 2);
	CameraBoom->SetRelativeRotation(tmpRotator);
	CameraBoom->bEnableCameraLag = CameraLag;
	CameraBoom->CameraLagSpeed = CameraLagIntensity;

	//Setting deltas for zoom method
	DeltaCameraDistance = (MaxCameraDistance - MinCameraDistance) / ZoomDivCount;

	DeltaCameraRotation.Pitch = (MaxCameraView.Pitch - MinCameraView.Pitch) / ZoomDivCount;
	DeltaCameraRotation.Yaw = (MaxCameraView.Yaw - MinCameraView.Yaw) / ZoomDivCount;
	DeltaCameraRotation.Roll = (MaxCameraView.Roll - MinCameraView.Roll) / ZoomDivCount;
}

void ASkillBox_22Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//TBR Setting rotation to cursor (���������� � ���������� ������)
	// if (APlayerController* PC = Cast<APlayerController>(GetController())) {
	// 	FHitResult HitResult;
	// 	PC->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);
	// 	if (IsValid(HitResult.GetActor()))
	// 	{
	// 		SetActorRotation(FRotator(.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw , .0f));
	// 	}
	// }
	
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		FVector MouseLocation, MouseDirection, IntersectionPoint;
		PC->DeprojectMousePositionToWorld(MouseLocation,MouseDirection);
		FVector PawnLocation = GetActorLocation();
		if (GetRayAndPlaneIntersection(MouseLocation,MouseDirection,5000,
			PawnLocation, FVector::UpVector,IntersectionPoint))
		{
			SetActorRotation(FRotator(0.f, UKismetMathLibrary::FindLookAtRotation(PawnLocation,IntersectionPoint).Yaw, 0.f));
		}
		
	}

	//Show decal at hit location
	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}



}

void ASkillBox_22Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// There are ways to bind a UInputAction* to a handler function and multiple types of ETriggerEvent that may be of interest.

		// This calls the handler function on the tick when MyInputAction starts, such as when pressing an action button.
		if (InputActions.MovementAction)
		{
			PlayerEnhancedInputComponent->BindAction(InputActions.MovementAction, ETriggerEvent::Triggered, this, &ASkillBox_22Character::Movement);
		}
		if (InputActions.WalkAction)
		{
			PlayerEnhancedInputComponent->BindAction(InputActions.WalkAction, ETriggerEvent::Started, this, &ASkillBox_22Character::StartWalking);
			PlayerEnhancedInputComponent->BindAction(InputActions.WalkAction, ETriggerEvent::Completed, this, &ASkillBox_22Character::StopWalking);
		}
		if (InputActions.AimAction)
		{
			PlayerEnhancedInputComponent->BindAction(InputActions.AimAction, ETriggerEvent::Started, this, &ASkillBox_22Character::StartAiming);
			PlayerEnhancedInputComponent->BindAction(InputActions.AimAction, ETriggerEvent::Completed, this, &ASkillBox_22Character::StopAiming);
		}

		if (InputActions.SprintAction)
		{
			PlayerEnhancedInputComponent->BindAction(InputActions.SprintAction, ETriggerEvent::Started, this, &ASkillBox_22Character::StartSprinting);
			PlayerEnhancedInputComponent->BindAction(InputActions.SprintAction, ETriggerEvent::Completed, this, &ASkillBox_22Character::StopSprinting);
		}
	}
}

void ASkillBox_22Character::Movement(const FInputActionValue& Value)
{

	if (Value.GetValueType() == EInputActionValueType::Axis3D) 
		OffsetDirection = Value.Get<FVector>();

	OffsetDirection.Normalize(.01);
	float tmpFactorValue = FVector::DotProduct(OffsetDirection, GetActorRotation().Vector());

	if ((tmpFactorValue >= 0) && (OffsetDirection.Length() > 0.1)) {
		OffsetDirection *= MovementSideFactor + (1 - MovementSideFactor) * tmpFactorValue;
	}
	else {
		OffsetDirection *= MovementSideFactor + (MovementSideFactor - MovementBackFactor) * tmpFactorValue;
	}

	AddMovementInput(OffsetDirection);
	
	//UKismetSystemLibrary::PrintString(this, OffsetDirection.ToString());
}

void ASkillBox_22Character::StartWalking(const FInputActionValue& Value)
{
	bIsWalking = true;
	ChangeMovementState();
}

void ASkillBox_22Character::StopWalking(const FInputActionValue& Value)
{
	bIsWalking = false;
	ChangeMovementState();
	
}

void ASkillBox_22Character::StartAiming(const FInputActionValue& Value)
{
	bIsAiming = true;
	ChangeMovementState();

}

void ASkillBox_22Character::StopAiming(const FInputActionValue& Value)
{
	bIsAiming = false;
	ChangeMovementState();
}

void ASkillBox_22Character::StartSprinting(const FInputActionValue& Value)
{
	bIsSprining = true;
	ChangeMovementState();

}

void ASkillBox_22Character::StopSprinting(const FInputActionValue& Value)
{
	bIsSprining = false;
	ChangeMovementState();
}

void ASkillBox_22Character::TimerFunction()
{
	float tmpCameraDistance = CameraBoom->TargetArmLength + (DeltaCameraDistance * ZoomValue / 40.f);
	FRotator tmpCameraView = CameraBoom->GetDesiredRotation() + (DeltaCameraRotation * (ZoomValue / 40.f));

	//UKismetSystemLibrary::PrintString(this, FString::SanitizeFloat(DeltaCameraDistance));
	//UKismetSystemLibrary::PrintString(this, FString::SanitizeFloat(ZoomDivCount));
	
	if ((tmpCameraDistance <= MaxCameraDistance) && (tmpCameraDistance >= MinCameraDistance)) {
		CameraBoom->TargetArmLength = tmpCameraDistance;
		CameraBoom->SetRelativeRotation(tmpCameraView);
	}
	else
	{
		GetWorldTimerManager().ClearTimer(ZoomTimerHandle);
		ZoomCycleStepNumber = 0;
		bCanZoom = true;
	}
	
	ZoomCycleStepNumber += 1;
	
	// Exes value to compensate for rounding error
	if (ZoomCycleStepNumber > 41)
	{
		GetWorldTimerManager().ClearTimer(ZoomTimerHandle);
		ZoomCycleStepNumber = 0;
		bCanZoom = true;
	}
	
}

void ASkillBox_22Character::CameraZoom(float Value)
{
	if (bCanZoom)
	{
		bCanZoom = false;
		ZoomValue = Value;
		GetWorldTimerManager().SetTimer(ZoomTimerHandle,this,&ASkillBox_22Character::TimerFunction, .003f, true);
	}
	else
	{
		//For smoother transition 
		ZoomCycleStepNumber = 0;
	}
}
