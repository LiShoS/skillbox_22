// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "GameFramework/Character.h"
#include "SkillBox_22/FuncLibrary/SupportTypes.h"
#include "SkillBox_22Character.generated.h"

UCLASS(Blueprintable)
class ASkillBox_22Character : public ACharacter
{
	GENERATED_BODY()

//Input private section

	UPROPERTY(EditDefaultsOnly)
	FCharacterInputActions InputActions;

	UPROPERTY(EditDefaultsOnly)
	UInputMappingContext* InputMappingContext;

//Movement private section

	bool bIsSprining = false;
	bool bIsWalking = false;
	bool bIsAiming = false;
	
	UPROPERTY(EditAnywhere, Category = "Movement")
		float MovementSideFactor = .8f;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float MovementBackFactor = .7f;

	FVector OffsetDirection = FVector::ZeroVector;

	UFUNCTION()
	void Movement(const FInputActionValue& Value);
	
	UFUNCTION()
    void StartWalking(const FInputActionValue& Value);

	UFUNCTION()
	void StopWalking(const FInputActionValue& Value);

	UFUNCTION()
	void StartAiming(const FInputActionValue& Value);

	UFUNCTION()
	void StopAiming(const FInputActionValue& Value);

	UFUNCTION()
	void StartSprinting(const FInputActionValue& Value);

	UFUNCTION()
	void StopSprinting(const FInputActionValue& Value);

//Camera private section
	bool bCanZoom = true;
	float DeltaCameraDistance;
	FRotator DeltaCameraRotation;
	FTimerHandle ZoomTimerHandle;


	//TBR Temp values for zoom (find beter way)
	float ZoomValue = 0;
	int32 ZoomCycleStepNumber = 0;
	
	void TimerFunction();
	
public:
	ASkillBox_22Character();

//Camera public section

	UFUNCTION()
	void CameraZoom(float Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	FRotator MaxCameraView;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	float MaxCameraDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	FRotator MinCameraView;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	float MinCameraDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	bool CameraLag = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	float CameraLagIntensity = 4.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
	int32 ZoomDivCount = 7;

// Character state update
	UPROPERTY()
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	
//Default Section

	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
};


