// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SkillBox_22 : ModuleRules
{
	public SkillBox_22(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"HeadMountedDisplay",
			"NavigationSystem",
			"AIModule",
			"Niagara",
			"EnhancedInput"
		});
    }
}
